function removeHideClass($Class, HideClass, show_time) {	
	setTimeout(function() { $Class.removeClass(HideClass); }, show_time);
};	
	$(document).ready(function() {
		removeHideClass($(".banner_img"), "hide", 500);
		removeHideClass($(".banner_text_title"), "hide", 1300);
		removeHideClass($(".banner_text_health"), "hide", 2000);
		removeHideClass($(".banner_text_intro"), "hide", 2500);
		removeHideClass($(".banner_text_button"), "hide", 2500);
	});	